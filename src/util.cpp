#include <titanium.hpp>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

using namespace titanium;

/*
 * Returns the path for the specified executable file.
 * Throws and exception if the file doesn't exist.
 */
static std::string get_path(const char *file)
{
	const char *str;

	if(!(str = getenv("PATH")))
		throw std::runtime_error(nullptr);
	const std::string path(str);
	std::stringstream stream(path);
	std::string s;

	while(std::getline(stream, s, ':'))
	{
		std::string p(s + "/" + file);
		struct stat statbuff;
		if(stat(p.c_str(), &statbuff) == 0)
			return p;
	}

	throw std::runtime_error(nullptr);
}

/*
 * Prints the specified command.
 */
static void print_command(const char *const *args)
{
	std::cout << "$> ";
	while(*args)
	{
		std::cout << *args;
		if(*++args)
			std::cout << ' ';
	}
	std::cout << '\n';
}

/*
 * Prints and executes the specified command and waits for it to end. The given earguments list is assumed to be
 * terminated by a null pointer.
 * Returns `true` on success, `false` on fail.
 */
bool titanium::exec_command(const char *const *args)
{
	std::string path;
	pid_t pid;
	int stat;

	if(!args || !args[0])
		return false;
	print_command(args);
	try
	{
		path = get_path(args[0]);
	}
	catch(const std::exception &)
	{
		std::cerr << "Not found: `" << args[0] << "`\n";
	}

	if((pid = fork()) < 0)
		return false;
	if(pid == 0)
	{
		if(execv(path.c_str(), (char *const *) args) < 0)
			exit(1);
	}

	if(waitpid(pid, &stat, 0) < 0)
		return false;
	if(WIFEXITED(stat) && WEXITSTATUS(stat) != 0)
		return false;
	if(WIFSIGNALED(stat))
		return false;
	return true;
}
