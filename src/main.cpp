#include <titanium.hpp>
#include <iostream>
#include <string.h>
#include <strings.h>

using namespace std;

/*
 * Prints command line help.
 */
static void print_help()
{
	std::cout << "Titanium unit tester version " << VERSION << '\n';
	std::cout << "usage: titanium [OPTIONS...]\n\n";
	std::cout << "\t-h, --help: Prints help\n\n";
	std::cout << "\t-d, --testsdir <path>: Sets the path for the directory containing the tests\n\n";
	std::cout << "\t-e, --exec <path>: Sets the path for the compiled executable\n\n";
	std::cout << "\t-l, --leak: Enables leak check. Every memory that is not freed is considered to be a leak\n\n";
	std::cout << "To be tested, the code must be precompiled and symbols must be present in the executable.\n";
}

/*
 * Parses flags in command line arguments.
 * `args` must beginning with the first argument, not the executable name.
 */
static titanium::flags parse_flags(char **args)
{
	titanium::flags f;

	while(*args)
	{
		if(strcmp(*args, "-h") == 0 || strcmp(*args, "--help") == 0)
			f.help = true;
		else if((strcmp(*args, "-d") == 0 || strcmp(*args, "--testsdir") == 0) && args[1])
		{
			f.tests_dir = args[1];
			++args;
		}
		else if((strcmp(*args, "-e") == 0 || strcmp(*args, "--exec") == 0) && args[1])
		{
			f.exec = args[1];
			++args;
		}
		else if(strcmp(*args, "-l") == 0 || strcmp(*args, "--leak") == 0)
			f.leak = true;
		++args;
	}
	return f;
}

/*
 * Builds a test report.
 * Returns `true` on success, `false` if at least one test has failed or if an error happened.
 */
static bool build_report(const titanium::flags &f)
{
	titanium::test_session s(f.tests_dir, f.exec);
	s.print_host_info();
	// TODO Allow to set compiler and flags
	s.print_compiler_info();

	s.print_test_suites_compile();
	s.print_test_suites_info();

	s.run_tests();
	s.print_tests_results();
	s.print_delta();

	s.print_conclusion();
	return s.success();
}

int main(int argc, char **argv)
{
	titanium::flags f;

	f = parse_flags(&argv[1]);
	if(argc <= 1 || f.help || f.tests_dir.empty() || f.exec.empty())
	{
		print_help();
		return 0;
	}
	return !build_report(f);
}
