#ifndef TITANIUM_HPP
# define TITANIUM_HPP

# define VERSION	"1.0"

# include <string>
# include <vector>

namespace titanium
{
	struct flags
	{
		bool help = false;
		std::string tests_dir;
		std::string exec;
		bool leak = false;
	};

	enum test_result
	{
		OK = '.',
		FAIL = 'F',
		COMPILATION = 'c',
		CRASH = 'C',
		LEAK = 'L',
		UNKNOWN = '?'
	};

	class test
	{
		public:
		inline test(const std::string &name)
			: name{name}
		{}

		inline const std::string &get_name() const
		{
			 return name;
		}

		void run();

		inline test_result get_result() const
		{
			return result;
		}

		void print_delta();
		void print_leaks();

		private:
		std::string name;
		test_result result = UNKNOWN;
	};

	class test_suite
	{
		public:
		inline test_suite(const std::string &file, const std::string &name, const std::string &description)
			: file{file}, name{name}, description{description}
		{}

		inline const std::string &get_file() const
		{
			return file;
		}

		inline const std::string &get_name() const
		{
			return name;
		}

		inline const std::string &get_description() const
		{
			return description;
		}

		inline std::vector<test> &get_tests()
		{
			return tests;
		}

		inline const std::vector<test> &get_tests() const
		{
			return tests;
		}

		void run();

		inline size_t total_tests() const
		{
			return tests.size();
		}

		size_t success_tests() const;

		test_result get_result() const;
		void print_result() const;

		private:
		std::string file, name, description;
		std::vector<test> tests;
	};

	class test_session
	{
		public:
		test_session(const std::string &tests_dir, const std::string &exec_path);
		test_session(const test_session &) = delete;

		void print_host_info() const;
		void print_compiler_info() const;

		void print_test_suites_compile();
		void print_test_suites_info() const;

		void run_tests();
		void print_tests_results() const;
		void print_delta() const;

		size_t total_tests() const;
		size_t success_tests() const;
		bool success() const;

		void print_conclusion() const;

		private:
		std::string tests_dir;
		std::string exec_path;

		std::vector<test_suite> suites;

		void print_section_header(const std::string &name) const;
		void print_section_end() const;
	};

	bool exec_command(const char *const *args);
}

#endif
