#include <titanium.hpp>
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <string.h>
#include <unistd.h>

using namespace titanium;

// TODO Add call to Makefile to compile project?
// TODO Add execution time (for both tests and whole session)

static char *new_tmp()
{
	char *buff;
	int fd;

	if(!(buff = strdup("/tmp/titanium_XXXXXXXX")) || (fd = mkstemp(buff)) < 0)
	{
		free(buff);
		return NULL;
	}
	close(fd);
	return buff;
}

test_session::test_session(const std::string &tests_dir, const std::string &exec_path)
	: tests_dir(tests_dir), exec_path(exec_path)
{}

/*
 * Prints informations about the testing host.
 */
void test_session::print_host_info() const
{
	print_section_header("Host");

	const char *args0[] = {"uname", "-msr", nullptr};
	exec_command(args0);
	const char *args1[] = {"date", nullptr};
	exec_command(args1);

	print_section_end();
}

/*
 * Prints informations about the compiler used to compile tests.
 */
void test_session::print_compiler_info() const
{
	print_section_header("Compiler");

	const char *args[] = {"g++", "-v", nullptr};
	exec_command(args);

	print_section_end();
}

/*
 * Retrieves the list of test suites and compiles them all.
 */
void test_session::print_test_suites_compile()
{
	print_section_header("Test suites compilation");

	const auto exec_name(new_tmp());
	const char *args[] = {"cp", exec_path.c_str(), exec_name, nullptr};
	exec_command(args);

	try
	{
		for(const auto &t : std::filesystem::directory_iterator(tests_dir))
		{
			if(!t.is_regular_file())
				continue;
			// TODO Check that file extention is .c or .cpp
			const auto obj_name(new_tmp());
			// TODO Change compiler and flags
			const char *args0[] = {"g++", "-c" , t.path().c_str(), "-o", obj_name, nullptr};
			exec_command(args0);
			const char *args1[] = {"strip", obj_name, nullptr};
			exec_command(args1);
			const auto test_name(new_tmp());
			// TODO Change compiler and flags
			const char *args2[] = {"g++", exec_name, obj_name, "-o", test_name, nullptr};
			exec_command(args2);
			// TODO Handle errors
			// TODO Add test suite to list
		}
	}
	catch(const std::exception &)
	{
		std::cerr << "Failed to open tests directory\n";
		// TODO Stop test session, exit with error
	}

	print_section_end();
}

/*
 * Prints informations about test suites.
 */
void test_session::print_test_suites_info() const
{
	print_section_header("Test suites informations");

	if(suites.empty())
		std::cout << "No test suites found\n";
	std::for_each(suites.cbegin(), suites.cend(), [](const auto &s)
		{
			std::cout << "- " << s.get_file() << " - Name: " << s.get_name();
			if(!s.get_description().empty())
				std::cout << " - Description: " << s.get_description();
			std::cout << " - " << s.total_tests() << " tests to be run\n";
		});

	print_section_end();
}

/*
 * Runs every test suites and stores results.
 */
void test_session::run_tests()
{
	std::for_each(suites.begin(), suites.end(), [](auto &s)
		{
			s.run();
		});
}

/*
 * Prints tests results in a compact form, ordered by test suite.
 */
void test_session::print_tests_results() const
{
	print_section_header("Test results");

	std::cout << ". = Ok, F = Failed, c = Compilation failed, C = Crash, L = Leak, ? = Unknown/not run\n\n";

	if(suites.empty())
		std::cout << "Nothing to run\n";
	std::for_each(suites.cbegin(), suites.cend(), [](const auto &s)
		{
			s.print_result();
		});

	print_section_end();
}

/*
 * Prints, for every test, the difference between the expected result and the actual result. Also prints leaks if found
 * and enabled.
 */
void test_session::print_delta() const
{
	print_section_header("Delta");

	if(suites.empty())
		std::cout << "Nothing to run\n";
	// TODO

	print_section_end();
}

/*
 * Prints the total number of tests.
 */
size_t test_session::total_tests() const
{
	size_t i(0);
	std::for_each(suites.cbegin(), suites.cend(), [&i](const auto &s)
		{
			i += s.total_tests();
		});
	return i;
}

/*
 * Returns the number of succeeded tests.
 */
size_t test_session::success_tests() const
{
	size_t i(0);
	std::for_each(suites.cbegin(), suites.cend(), [&i](const auto &s)
		{
			if(s.get_result() == OK)
				++i;
		});
	return i;
}

/*
 * Returns `false` if a at least one test failed or if an error happened. Returns `true` if not.
 */
bool test_session::success() const
{
	return success_tests() >= total_tests();
}

/*
 * Prints the conclusion of the report.
 */
void test_session::print_conclusion() const
{
	print_section_header("Conclusion");

	if(total_tests() == 0)
		std::cout << "No test suites found, nothing to test\n";
	else if(success())
		std::cout << "All tests are passing (" << total_tests() << "/" << total_tests() << ")\n";
	else
	{
		std::cout << "Passing tests: (" << success_tests() << "/" << total_tests() << ")\n";
		std::cout << "Not passing\n";
	}

	print_section_end();
}

/*
 * Prints the header of a report section.
 */
void test_session::print_section_header(const std::string &name) const
{
	std::cout << "--- " << name << " ---\n\n";
}

/*
 * Prints the end of a report section.
 */
void test_session::print_section_end() const
{
	std::cout << "\n\n\n";
}
