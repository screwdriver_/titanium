#include <titanium.hpp>

#include <algorithm>
#include <iostream>

using namespace titanium;

/*
 * Runs every tests.
 */
void test_suite::run()
{
	// TODO
}

size_t test_suite::success_tests() const
{
	size_t i(0);
	std::for_each(tests.cbegin(), tests.cend(), [&i](const auto &t)
		{
			if(t.get_result() == OK)
				++i;
		});
	return i;
}

/*
 * Returns the result for of the test suite. OK if every tests succeeded or FAIL if at least one test did not succeed.
 */
test_result test_suite::get_result() const
{
	const auto success(std::all_of(tests.cbegin(), tests.cend(), [](const auto &t)
		{
			return (t.get_result() == OK);
		}));
	return (success ? OK : FAIL);
}

/*
 * Prints the results for every tests.
 */
void test_suite::print_result() const
{
	size_t success(0);

	std::cout << '[' << get_result() << "] ";
	for(const auto &t : tests)
	{
		const auto r(t.get_result());
		if(r == OK)
			++success;
		std::cout << r;
	}
	std::cout << " (" << success << '/' << tests.size() << ")\n";
}
