NAME = titanium
CC = g++
CFLAGS = -Wall -Wextra -Werror -Wno-unused-result -g3 -D DEBUG -std=c++17

SRC_DIR = src/
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")
HDR := $(shell find $(SRC_DIR) -type f -name "*.hpp")
DIR := $(shell find $(SRC_DIR) -type d)

OBJ_DIR = obj/
OBJ := $(patsubst $(SRC_DIR)%.cpp,$(OBJ_DIR)%.o,$(SRC))
OBJ_DIRS := $(patsubst $(SRC_DIR)%,$(OBJ_DIR)%,$(DIR))

INCLUDES = -I $(SRC_DIR)

all: tags $(NAME)

$(OBJ_DIRS):
	mkdir -p $(OBJ_DIRS)

$(NAME): $(OBJ_DIRS) $(OBJ)
	$(CC) $(CFLAGS) -o $(NAME) $(OBJ)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp $(HDR)
	$(CC) $(CFLAGS) -o $@ -c $< $(INCLUDES)

tags: $(SRC) $(HDR)
	ctags $(SRC) $(HDR)

clean:
	rm -rf $(OBJ_DIR)

fclean: clean
	rm -f $(NAME)
	rm -f tags

re: fclean all

.PHONY: all clean fclean re
